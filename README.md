WFZoomSideMenu
==============


This component is the Android implementation of the TWTSideMenuViewController component.
Use this stuff as the main Activity of your application.

Features:
-----------
 * Support of multiple screen size
 * Support of orientation change
 * Adjustable master width in pixels and in percent.
 * Adjustable detail slipping out in pixels and in percent.
 * Support of fling (swipe) gesture, to open and close the menu.

Usage:
-----------
 Use one of the setup methods to initialize the component.
 Use these constants to reach the master and detail views to load fragments in:
 * WFZoomSideMenuActivity.MASTER_ID
 * WFZoomSideMenuActivity.DETAIL_ID
