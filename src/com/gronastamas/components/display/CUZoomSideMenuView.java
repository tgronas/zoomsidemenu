package com.gronastamas.components.display;

import android.content.Context;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.gronastamas.components.gestures.FlingDetector;

/**
 * <p>
 *     Custom view represents the component's root view, the container.
 * </p>
 * <h3>Features:</h3>
 * <p>
 *     <ul>
 *         <li>The view's layout creation.</li>
 *         <li>Making difference between fling touch event and other events.</li>
 *     </ul>
 * </p>
 * <p>
 *     Created by gronastamas on 27/05/2014.
 * </p>
 */
public class CUZoomSideMenuView extends RelativeLayout
{
	private int mTouchSlop;
	private float mDownX;
	private FlingDetector mFlingDetector;
	private OnLayoutSetupListener mListener;
	private ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener;
	private ImageView mBackgroundView;

	/**
	 * Implements the activity class to listen
	 * the event, which fired when the the layout ad been setup.
	 * This is important, because we get here the actual width and height of the view.
	 */
	public interface OnLayoutSetupListener
	{
		public void onLayoutSetup(int viewWidth, int viewHeight);
	}

	/**
	 * The constructor of this component.
	 * @param context The Activity of this view.
	 */
	public CUZoomSideMenuView(Context context)
	{
		super(context);

		mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
		mBackgroundView = new ImageView(context);

		try
		{
			mListener = (OnLayoutSetupListener) context;
		}
		catch(ClassCastException exception)
		{
			Log.e("Error", context.toString() + " can't cast to " + mListener.getClass());
		}

		addView(mBackgroundView);

		setupLayoutListener();
		setupLayout();
	}

	/**
	 * Passes the FlingDetector class.
	 * @param flingDetector Detects the fling gestures.
	 */
	public void setFlingDetector(FlingDetector flingDetector)
	{
		mFlingDetector = flingDetector;
	}

	/**
	 * Set the container's background image using this method.
	 * @param resource The background drawable.
	 */
	public void setBackgroundImage(BitmapDrawable resource, String type)
	{
		if(resource == null)
			return;

		if(type.equals(CUZoomSideMenuActivity.CENTERED))
		{
			mBackgroundView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			mBackgroundView.setAdjustViewBounds(false);
			mBackgroundView.setBackgroundDrawable(resource);


		}
		else if( type.equals(CUZoomSideMenuActivity.TILED) )
		{
			resource.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
			mBackgroundView.setBackgroundDrawable(resource);
		}
		else
		{
			Log.e("Error.", type+" is not a valid type.");
		}
	}

	/**
	 * This method gets earliest any touch events.
	 * We decide here whether a fling actually happened,
	 * and the fling starting x coordinate also passed here to the
	 * FlingDetector instance.
	 * @param event The MotionEvent instance.
	 * @return true if a fling happened, otherwise false.
	 */
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event)
	{
		final int action = event.getActionMasked();

		switch (action)
		{
			case MotionEvent.ACTION_DOWN:
				mDownX = event.getRawX();
				mFlingDetector.setLastOnDownEvent(event.getRawX());
				return false;
			case MotionEvent.ACTION_MOVE:
				float deltaX = event.getRawX() - mDownX;
				if (Math.abs(deltaX) > mTouchSlop)
					return true;
				break;
		}
		return super.onInterceptTouchEvent(event);
	}

	private void setupLayout()
	{
		final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

		mBackgroundView.setLayoutParams(layoutParams);

	}

	private void setupLayoutListener()
	{
		mGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener()
		{
			@Override
			public void onGlobalLayout()
			{
				getViewTreeObserver().removeGlobalOnLayoutListener(mGlobalLayoutListener);
				mListener.onLayoutSetup(getWidth(), getHeight());
			}
		};
		getViewTreeObserver().addOnGlobalLayoutListener(mGlobalLayoutListener);
	}
}