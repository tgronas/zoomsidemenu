package com.gronastamas.components.display;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * <p>
 *     Creates a rectange with a shadow around.
 *     This component addresses the following issue:
 *     When the target API level is 14 or higher, the shadow layer doesn't work
 *     in hardware mode. This component is rendered in software mode, that's why
 *     the detail view can be rendered in hardware mode for better performance.
 * </p>
 * <p>
 *     Created by gronastamas on 28/05/2014.
 * </p>
 */
public class ShadowImage extends View
{
	private final static Paint shadowPaint = new Paint();

	/**
	 * The constructor of this component.
	 * @param context The Activity of this view.
	 */
	public ShadowImage(Context context)
	{
		super(context);

		shadowPaint.setShadowLayer(15.0f, 0.0f, 0.0f, Color.BLACK);

		setLayerType(View.LAYER_TYPE_SOFTWARE,null);
		setWillNotDraw(false);
	}

	/**
	 * Draws a drop shadow around a rect, which fills the whole component.
	 * @param canvas The canvas of this view.
	 */
	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawRect(15,15,getWidth()-15,getHeight()-15,shadowPaint);
	}
}
