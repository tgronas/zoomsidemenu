package com.gronastamas.components.display;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.gronastamas.components.gestures.FlingDetector;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 *     This component is the Android implementation of the TWTSideMenuViewController.
 *     Use this stuff as the main Activity of your application.
 * </p>
 * <h3>Features:</h3>
 * <p>
 *     <ul>
 *         <li>Support of multiple screen size</li>
 *         <li>Support of orientation change</li>
 *         <li>Adjustable master width in pixels and in percent.</li>
 *         <li>Adjustable detail slipping out in pixels and in percent.</li>
 *         <li>Support of fling (swipe) gesture, to open and close the menu.</li>
 *     </ul>
 * </p>
 * <h3>Usage:</h3>
 * <p>
 *     Use one of the setup methods to initialize the component.
 *     Use these constants to reach the master and detail views to load fragments in:
 *     <ul>
 *         <li>WFZoomSideMenuActivity.MASTER_ID</li>
 *         <li>WFZoomSideMenuActivity.DETAIL_ID</li>
 *     </ul>
 * </p>
 *
 * <p>
 *     Created by gronastamas on 21/05/2014.
 * </p>
 */
public class CUZoomSideMenuActivity extends Activity  implements	Animator.AnimatorListener,
																	View.OnClickListener,
																	FlingDetector.OnMenuFlingListener,
																	DetailView.OnCloseMenuButtonClickListener,
																	CUZoomSideMenuView.OnLayoutSetupListener
{
	private final static AtomicInteger UNIQUE_ID_GENERATOR = new AtomicInteger(Integer.MAX_VALUE);
	private final static int CONTAINER_ID = UNIQUE_ID_GENERATOR.decrementAndGet();

	/** Indicates the version of the component. */
	public final static String VERSION = "1.0.3";

	/** The id of the master view. Use it to reach this view. */
	public final static int MASTER_ID = UNIQUE_ID_GENERATOR.decrementAndGet();

	/** The id of the content container inside the detail view. Use it to reach this view. */
	public final static int DETAIL_ID = UNIQUE_ID_GENERATOR.decrementAndGet();

	/** Defines a constant for the centered background image type. */
	public final static String CENTERED = "centered";

	/** Defines a constant for the tiled background image type. */
	public final static String TILED = "tiled";

	private int mScreenWidth;
	private int mScreenHeight;
	private int mGapBetweenMasterAndDetail = 200;
	private float mMasterWidthInPercent = -1;
	private int mDetailSlipOut = 200;
	private float mDetailSlipOutInPercent = -1;
	private long mAnimationDuration = 200;
	private BitmapDrawable mBackgroundImage;
	private Boolean mIsMenuEnabled = true;
	private Boolean mIsMenuOpened = false;
	private Boolean mIsAnimationInProgress = false;
	private Boolean mIsLayoutSetupInitialized = false;
	private CUZoomSideMenuView mContainer;
	private FrameLayout mMaster;
	private DetailView mDetail;
	private FlingDetector mFlingDetector;
	private GestureDetector mGestureDetector;
	private String mBackgroundType;

	/**
	 * Initializes the component.
	 * @param gapBetweenMasterAndDetail Determines the gap between the master and the detail view in pixels.
	 * @param detailSlipOut Determines the slipping out value for the detail view. Used when the menu is opened.
	 * @param animationDuration The duration of the menu animation.
	 * @param backgroundImage The Drawable resource for the background image.
	 * @param backgroundType Embed type of the bg image, can be centered or tiled.
	 */
	public void setup(int gapBetweenMasterAndDetail, int detailSlipOut, long animationDuration, BitmapDrawable backgroundImage, String backgroundType)
	{
		mGapBetweenMasterAndDetail = gapBetweenMasterAndDetail;
		mDetailSlipOut = detailSlipOut;
		mAnimationDuration = animationDuration;
		mBackgroundImage = backgroundImage;
		mBackgroundType = backgroundType;

		if(mIsLayoutSetupInitialized)
			setupLayout();
	}

	/**
	 * Initializes the component.
	 * @param masterWidthInPercent Determines the width of the master view in percent.
	 *                             Calculated from the empty space near the detail view.
	 *                             For example when this value is 100%, the master view fills the entire area
	 *                             from the left side of the screen, to the detail view (when the menu is opened).
	 * @param detailSlipOutInPercent Determines the slipping out value for the detail view in percent.
	 *                               This value is based on the width of the detail view.
	 * @param animationDuration The duration of the menu animation.
	 * @param backgroundImage The Drawable resource for the background image.
	 * @param backgroundType Embed type of the bg image, can be centered or tiled.
	 */
	public void setup(float masterWidthInPercent, float detailSlipOutInPercent, long animationDuration, BitmapDrawable backgroundImage, String backgroundType)
	{
		if(masterWidthInPercent < 0 || masterWidthInPercent > 100)
		{
			Log.e("Error", "gapBetweenMasterAndDetailInPercent value cannot be greater tha 100 or smaller than 0.");
			return;
		}
		if(detailSlipOutInPercent < 0 || detailSlipOutInPercent > 100)
		{
			Log.e("Error", "detailSlipOutInPercent value cannot be greater tha 100 or smaller than 0.");
			return;
		}

		mMasterWidthInPercent = masterWidthInPercent;
		mDetailSlipOutInPercent = detailSlipOutInPercent;
		mAnimationDuration = animationDuration;
		mBackgroundImage = backgroundImage;
		mBackgroundType = backgroundType;

		if(mIsLayoutSetupInitialized)
			setupLayout();
	}

	/**
	 * Indicates if the menu is enabled or not.
	 * @return Returns true if the menu is enabled, false if disabled.
	 */
	public boolean getMenuEnabled()
	{
		return mIsMenuEnabled;
	}

	/**
	 * Determines if the menu is enabled or not.
	 * @param enabled If false, the menu cannot be used.
	 */
	public void setMenuEnabled(boolean enabled)
	{
		mIsMenuEnabled = enabled;
	}

	/**
	 * Indicates whether the menu is opened or not.
	 * @return true if the menu is opened, othervise false.
	 */
	public boolean isMenuOpened()
	{
		return mIsMenuOpened;
	}

	/**
	 * Use this method to open and close the menu.
	 * Using this is really simple: if the menu is closed, opens it, otherwise close it.
	 */
	public void setMenuOpened()
	{
		if(!mIsMenuEnabled)
			return;

		if (mIsAnimationInProgress)
			return;

		float containerX;
		PropertyValuesHolder contentProperty1;
		PropertyValuesHolder contentProperty2;


		if (mIsMenuOpened)
		{
			mDetail.setContentEnabled(true);
			containerX = mContainer.getX() - mDetailSlipOut;
			contentProperty1 = PropertyValuesHolder.ofFloat("scaleX", 1f);
			contentProperty2 = PropertyValuesHolder.ofFloat("scaleY", 1f);
			mIsMenuOpened = false;
		}
		else
		{
			mDetail.setContentEnabled(false);
			containerX = mContainer.getX() + mDetailSlipOut;
			contentProperty1 = PropertyValuesHolder.ofFloat("scaleX", 0.5f);
			contentProperty2 = PropertyValuesHolder.ofFloat("scaleY", 0.5f);
			mIsMenuOpened = true;
		}
		final PropertyValuesHolder contentProperty3 = PropertyValuesHolder.ofFloat("x", containerX);

		final ValueAnimator contentAnimator = ObjectAnimator.ofPropertyValuesHolder(mContainer, contentProperty1, contentProperty2, contentProperty3);
							contentAnimator.setDuration(mAnimationDuration);
							contentAnimator.setInterpolator(new AccelerateInterpolator(0.5f));
							contentAnimator.addListener(this);
							contentAnimator.start();
	}

	private void setupLayout()
	{
		if(mDetailSlipOutInPercent != -1 && mMasterWidthInPercent != -1)
		{
			mDetailSlipOut = mScreenWidth*(int)mDetailSlipOutInPercent/200;
			mGapBetweenMasterAndDetail = mScreenWidth*(int)mMasterWidthInPercent/100-(mScreenWidth/2-mDetailSlipOut);
		}
		else
		{
			mGapBetweenMasterAndDetail = mScreenWidth / 2 - mGapBetweenMasterAndDetail;
		}
		final int detailLeftMargin 			= mScreenWidth + mDetailSlipOut*2;
		final int containerLeftMargin 		= detailLeftMargin*-1;
		final int masterTopBottomMargins 	= (mScreenHeight*2 - mScreenHeight)/2;
		final int containerTopBottomMargins = masterTopBottomMargins*-1;

		final RelativeLayout.LayoutParams 	detailLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
											detailLayoutParams.setMargins(detailLeftMargin, masterTopBottomMargins, 0, masterTopBottomMargins);

		final FrameLayout.LayoutParams 	containerLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
										containerLayoutParams.setMargins(containerLeftMargin, containerTopBottomMargins, 0, containerTopBottomMargins);

		final RelativeLayout.LayoutParams 	masterLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
											masterLayoutParams.width = mGapBetweenMasterAndDetail;
											masterLayoutParams.height = mScreenHeight;

		mDetail.setPivotX(0);
		mDetail.setPivotY(mScreenHeight);
		mDetail.setLayoutParams(detailLayoutParams);
		mDetail.setDrawingCacheEnabled(true);
		mDetail.setDrawingCacheQuality(1);

		mContainer.setPivotX(mScreenWidth - containerLeftMargin);
		mContainer.setPivotY(mScreenHeight);
		mContainer.setLayoutParams(containerLayoutParams);

		mMaster.setPivotX(0);
		mMaster.setPivotY(0);
		mMaster.setScaleX(2f);
		mMaster.setScaleY(2f);
		mMaster.setLayoutParams(masterLayoutParams);
		mMaster.setDrawingCacheEnabled(true);
		mMaster.setDrawingCacheQuality(1);

		mIsLayoutSetupInitialized = true;


	}

	/** {@inheritDoc} */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mContainer = new CUZoomSideMenuView(this);
		mContainer.setId(CONTAINER_ID);

		setContentView(mContainer);

		mFlingDetector = new FlingDetector(this);
		mGestureDetector = new GestureDetector(this, mFlingDetector);

		mMaster = new FrameLayout(this);
		mMaster.setId(MASTER_ID);

		mDetail = new DetailView(this);
		mDetail.setContainerId(DETAIL_ID);

		mContainer.setFlingDetector(mFlingDetector);
		mContainer.setBackgroundImage(mBackgroundImage, mBackgroundType);
		mContainer.addView(mMaster);
		mContainer.addView(mDetail);
	}

	/**
	 * Invoked, when the view's draw process completed.
	 * @param viewWidth The actual width of the component.
	 * @param viewHeight The actual width of the component.
	 */
	@Override
	public void onLayoutSetup(int viewWidth, int viewHeight)
	{
		mScreenWidth = viewWidth;
		mScreenHeight = viewHeight;
		setupLayout();
	}

	/** {@inheritDoc} */
	@Override
	public void onAnimationStart(Animator animation)
	{
		mIsAnimationInProgress = true;
	}

	/** {@inheritDoc} */
	@Override
	public void onAnimationEnd(Animator animation)
	{
		mIsAnimationInProgress = false;
	}

	/** {@inheritDoc} */
	@Override
	public void onAnimationCancel(Animator animation) { }

	/** {@inheritDoc} */
	@Override
	public void onAnimationRepeat(Animator animation) { }

	/** {@inheritDoc} */
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		return mGestureDetector.onTouchEvent(event);
	}

	/**
	 * Invoked, when a fling gesture occured.
	 * @param direction The direction of the fling (left or right).
	 */
	@Override
	public void onMenuFling(int direction)
	{
		if(mIsAnimationInProgress)
			return;

		if(mIsMenuOpened && direction == FlingDetector.LEFT)
			setMenuOpened();
		else if(!mIsMenuOpened && direction == FlingDetector.RIGHT)
			setMenuOpened();
	}

	/**
	 * Invoked, when the content disable button is pressed
	 * over the detail view.
	 * Closes the menu.
	 */
	@Override
	public void onCloseMenuButtonClicked()
	{
		setMenuOpened();
	}

	/** {@inheritDoc} */
	@Override
	public void onClick(View v)
	{
		setMenuOpened();
	}
}