package com.gronastamas.components.display;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

/**
 * <p>
 *     Custom detail view. Used to handle the states and events of
 *     the content disable button, and the background shadow.
 * </p>
 * <p>
 *     Created by gronastamas on 27/05/2014.
 * </p>
 */
public class DetailView extends FrameLayout implements View.OnClickListener
{
	private FrameLayout mContainer;
	private Button mContentDisableButton;
	private ShadowImage shadowImage;
	private OnCloseMenuButtonClickListener mListener;

	/**
	 * Implements the activity class to listen
	 * to the content disable button's event.
	 * This button is an invisible one, and covers the entire view,
	 * when the menu is opened.
	 * During this time the content of the detail view is disabled.
	 */
	public interface OnCloseMenuButtonClickListener
	{
		public void onCloseMenuButtonClicked();
	}

	/**
	 * Constructor of this class.
	 * @param context The activity class.
	 */
	public DetailView(Context context)
	{
		super(context);

		mContainer = new FrameLayout(context);

		mContentDisableButton = new Button(context);
		mContentDisableButton.setAlpha(0f);
		mContentDisableButton.setVisibility(View.GONE);
		mContentDisableButton.setOnClickListener(this);

		shadowImage = new ShadowImage(context);

		try
		{
			mListener = (OnCloseMenuButtonClickListener) context;
		}
		catch (ClassCastException exception)
		{
			Log.e("Error", context.toString() + " can't cast to " + mListener.getClass());
		}

		addView(shadowImage);
		addView(mContainer);
		addView(mContentDisableButton);

		setupLayout();
	}

	/**
	 * Sets the id of the container.
	 * @param id The id of the content container.
	 */
	public void setContainerId(int id)
	{
		mContainer.setId(id);
	}

	/**
	 * Sets the content of the detail view to enabled or disabled.
	 * @param enabled Determines if the content is enabled or not.
	 *                If true, the content disable button is invisible, that's why the content is enabled,
	 *                otherwise the content disable button is visible (actually not, because it's alpha is set to 0),
	 *                so the content is disabled.
	 */
	public void setContentEnabled(boolean enabled)
	{
		mContentDisableButton.setVisibility(enabled ? View.GONE : View.VISIBLE);

		if(!enabled)
		{
			mContainer.setPadding(15, 15, 15, 15);
			shadowImage.setVisibility(View.VISIBLE);
		}
		else
		{
			mContainer.setPadding(0, 0, 0, 0);
			shadowImage.setVisibility(View.GONE);
		}
	}

	private void setupLayout()
	{
		final FrameLayout.LayoutParams 	layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

		shadowImage.setLayoutParams(layoutParams);
		mContainer.setLayoutParams(layoutParams);
		mContentDisableButton.setLayoutParams(layoutParams);
	}

	/** {@inheritDoc} */
	@Override
	public void onClick(View v)
	{
		mListener.onCloseMenuButtonClicked();
	}
}