package com.gronastamas.components.gestures;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * <p>
 *     Detects fling events, and decides it's direction.
 *     Left or right is acceptable with a minimum distance,
 *     otherwise the fling is ignored.
 * </p>
 * <p>
 *     Created by gronastamas on 26/05/2014.
 * </p>
 */
public class FlingDetector extends GestureDetector.SimpleOnGestureListener
{
	/** Defines a constant to determine the left direction of the fling gesture.  */
	public static final int LEFT  = 0;

	/** Defines a constant to determine the right direction of the fling gesture.  */
	public static final int RIGHT = 1;

	private static final int MINIMUM_FLING_DISTANCE = 50;

	private OnMenuFlingListener mListener;
	private float mLastDownX;

	/**
	 * Implements the activity class to listen
	 * to the custom fling event.
	 * This event is dispatched, when a fling happened in felt or right direction.
	 */
	public interface OnMenuFlingListener
	{
		public void onMenuFling(int direction);
	}

	/**
	 * Constructor of this class.
	 * @param context The activity class.
	 */
	public FlingDetector(Context context)
	{
		try
		{
			mListener = (OnMenuFlingListener) context;
		}
		catch (ClassCastException exception)
		{
			Log.e("Error", context.toString() + " can't cast to " + mListener.getClass());
		}
	}

	/**
	 * Sets the last x value when the screen is tapped and the fling gesture started.
	 * This is a hack to address an issue, when the screen is tapped is not interpreted
	 * as a touch event, it should be handled as a click event, otherwise the buttons in the
	 * app doesn't work, that's why the onFling method doesn't get the event1 parameter,
	 * and we don't know the start value of the fling.
	 * @param lastDownX Last x value when the screen is tapped. The start value of the fling gesture.
	 *
	 */
	public void setLastOnDownEvent( float lastDownX )
	{
		mLastDownX = lastDownX;
	}

	/**
	 * Handles the fling, decides it's direction, and dispatches an event to the activity.
	 * @param event1 Starting touch event. Null in our case, because the down event is reserved for other click events.
	 * @param event2 The end point of the fling.
	 * @param velocityX Velocity of the fling to x direction.
	 * @param velocityY Velocity of the fling to y direction.
	 * @return true
	 */
	@Override
	public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
	{
		if( mLastDownX+MINIMUM_FLING_DISTANCE < event2.getRawX() )
			mListener.onMenuFling(RIGHT);
		else if( mLastDownX-MINIMUM_FLING_DISTANCE > event2.getRawX() )
			mListener.onMenuFling(LEFT);

		return true;
	}
}